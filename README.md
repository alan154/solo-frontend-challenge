# solo-frontend-challenge

This challenge will test your frontend skills.

# Purpose
Aim of this test is three fold,

- evaluate your coding abilities 
- judge your technical experince
- understand how you design a solution

# How you will be judged
You will be scored on,

- coding standard, comments and style
- overall solution design
- appropriate use of source control
- clean code architecture

# What you need to do

For this project you will create a TODO manager in one the following JS Frameworks (React, Vue, you can use Angular but preferably the other ones should be used), where the user will be able to create, retreive, update and delete todos. In the folder in this repo you will see more detailed instructions and goals for the Frontend section.

# Intructions

- [ ] Use a JS Framework for the FrontEnd (React, Vue, you can use Angular but preferably the other ones should be used).
- [ ] Create a User friendly interface.
- [ ] Upload the code to a repository and make it public.
- [ ] Add instructions to run the project so we can evaluate it.

## Details

- You will have a maximun of 72 hours to complete this challenge.
- You should send an email with the repository link and your name to [engineering@solotrucking.com](mailto:engineering@solotrucking.com).
- We will send you a confirmation email once it is in our inbox, if you dont receive the confimartion email please contact with the recruiter.
- We are going yo give you feedback few days after we receive your email.
- Please make a README.md file with the instructions to run the project.
- Use any other third party library of your choice if needed.
