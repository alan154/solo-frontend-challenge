# Important
*  For this challenge you need to use the following plubic API to list, create, udpate and delete the todos you can find the documentation in the following link: [JSON placeholder](https://jsonplaceholder.typicode.com/)

## Instructions
You should use one of the following javascript frameworks:
* React
* Vue
* Angular

You can use any other third party libraries of your choice if needed. 

### Goals
* List all Todos on screen, when te page loads.
* Creation of new Todos.
* Update of existing Todos.
* Deletion of Todos.
* Have resusable components.
* Needs to be responsive.
* User friendly.

### Strech Goals (Not necessary for the evaluation, but will give you bonus points)
* Global State Management (Redux, Vuex, etc...).
* Styled UI.
* Unit Testing.
* Use React Native or Ionic Framework (10 bonus points for Gryffindor if you use it).
